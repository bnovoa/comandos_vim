![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

![Vim logo](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11533962/Vimlogo.png)

Recopilación de los comandos del editor Vim más útiles y quizás más utilizados.

La página se puede consultar en la siguiente url:
* https://victorhck.gitlab.io/comandos_vim/

Esta página es una traducción y adaptación que he realizado de una página en inglés que se puede consultar en la siguiente url:

* http://vimsheet.com/
 

